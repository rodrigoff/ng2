# Angular2 + GitHub API

Angular2 integration with GitHub API.

Based off of [angular2-toh](https://github.com/rodrigoff/angular2-toh) sample app from [angular.io](https://angular.io/docs/ts/latest/tutorial/).

## Planned features

- Users list
- User detail
    - User info
    - User repositories
    - User gists
- Repositories list
- Repository detail

## Commands

```
npm start - runs the compiler and a server at the same time, both in "watch mode"
npm run tsc - runs the TypeScript compiler once
npm run tsc:w - runs the TypeScript compiler in watch mode; the process keeps running, awaiting changes to TypeScript files and re-compiling when it sees them
npm run lite - runs the lite-server, a light-weight, static file server with excellent support for Angular apps that use routing
npm run typings - runs the typings tool
npm run postinstall - called by npm automatically after it successfully completes package installation. This script installs the TypeScript definition files defined in typings.json
```

## Reference
- [Angular.io](https://angular.io/)
- [TypeScript](http://www.typescriptlang.org/)
