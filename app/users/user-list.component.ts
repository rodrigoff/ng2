import { Component, OnInit } from 'angular2/core';
import { Router } from 'angular2/router';

import { User } from './user';
import { UserService } from './user.service';

@Component({
    selector: 'user-list',
    templateUrl: 'app/users/templates/user-list.component.html',
    styleUrls: ['app/users/styles/user-list.component.css']
})

export class UserListComponent implements OnInit{
    users: User[] = [];

    constructor(private _router: Router,
        private _userService: UserService) { }

    ngOnInit() {
        this.getUsers();
    }

    getUsers() {
        this._userService.getUsers()
            .subscribe(users => this.users = users);
    }

    gotoUserDetail(username: string) {
        this._router.navigate(['UserDetail', { username: username }]);
    }
}
