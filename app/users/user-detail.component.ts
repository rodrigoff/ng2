import { Component, OnInit } from 'angular2/core';
import { RouteParams } from 'angular2/router';

import { User } from './user';
import { UserService } from './user.service';

@Component({
    selector: 'user-detail',
    templateUrl: 'app/users/templates/user-detail.component.html',
    styleUrls: ['app/users/styles/user-detail.component.css']
})

export class UserDetailComponent implements OnInit {
    user: User;

    constructor(private _routeParams: RouteParams,
        private _userService: UserService) { }

    ngOnInit() {
        let login = this._routeParams.get('username');
        this.getUser(login);
    }

    getUser(login: string) {
        this._userService.getUser(login)
            .subscribe(user => this.user = user);
    }

    goBack() {
        window.history.back();
    }
}
