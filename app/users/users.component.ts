import { Component } from 'angular2/core';
import { RouteConfig, RouterOutlet } from 'angular2/router';

import { User } from './user';
import { UserService } from './user.service';
import { UserListComponent } from './user-list.component';
import { UserDetailComponent } from './user-detail.component';

@Component({
    selector: 'users',
    templateUrl: 'app/users/templates/users.component.html',
    directives: [RouterOutlet],
    providers: [UserService]
})

@RouteConfig([
    {
        path: '/',
        name: 'UserList',
        component: UserListComponent,
        useAsDefault: true
    },
    {
        path: '/:username',
        name: 'UserDetail',
        component: UserDetailComponent
    }
])

export class UsersComponent { }
