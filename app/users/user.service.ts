import { Injectable } from 'angular2/core';
import { Http, Response } from 'angular2/http';
import { Observable } from 'rxjs/Observable';

import { User } from './user';

@Injectable()
export class UserService {
    private _url = 'https://api.github.com/users';

    constructor (private http: Http) { }

    getUsers() {
        return this.http.get(this._url)
            .map(response => <User[]> response.json())
            .catch(this.handleError);
    }

    getUser(username: string) {
        return this.http.get(`${this._url}/${username}`)
            .map(response => <User> response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
