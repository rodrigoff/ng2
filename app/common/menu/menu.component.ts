import { Component } from 'angular2/core';
import { ROUTER_DIRECTIVES } from 'angular2/router';

@Component({
    selector: 'menu',
    templateUrl: 'app/common/menu/menu.component.html',
    styleUrls: ['app/common/menu/menu.component.css'],
    directives: [ROUTER_DIRECTIVES]
})

export class MenuComponent { }
