import { Component } from 'angular2/core';
import { RouteConfig, RouterOutlet } from 'angular2/router';

import { RepositoryListComponent } from './repository-list.component';

@Component({
    selector: 'repositories',
    templateUrl: 'app/repositories/templates/repositories.component.html',
    directives: [RouterOutlet]
})

@RouteConfig([
    {
        path: '/',
        name: 'RepositoryList',
        component: RepositoryListComponent,
        useAsDefault: true
    }
])

export class RepositoriesComponent { }
