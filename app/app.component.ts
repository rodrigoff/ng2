import { Component } from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import { HTTP_PROVIDERS } from 'angular2/http';

import { MenuComponent } from './common/menu/menu.component';
import { HomeComponent} from './home/home.component'
import { UsersComponent } from './users/users.component';
import { RepositoriesComponent } from './repositories/repositories.component';

@Component({
    selector: 'my-app',
    templateUrl: 'app/templates/app.component.html',
    directives: [
        MenuComponent,
        ROUTER_DIRECTIVES
    ],
    providers: [
        ROUTER_PROVIDERS,
        HTTP_PROVIDERS
    ]
})

@RouteConfig([
    {
        path: '/home',
        name: 'Home',
        component: HomeComponent,
        useAsDefault: true
    },
    {
        path: '/users/...',
        name: 'Users',
        component: UsersComponent
    },
    {
        path: '/repositories/...',
        name: 'Repositories',
        component: RepositoriesComponent
    }
])

export class AppComponent {
    title = 'Angular2 + GitHub API';
}
